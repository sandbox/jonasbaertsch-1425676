<?php

/**
 * @file
 * Provides theme functions for the employee performance module
 */

function theme_employee_performance_table($data, $year, $month, $person_id, $person_name) {
  drupal_add_css(drupal_get_path('module', 'employee_performance') .'/employee_performance.css', 'module', 'all', FALSE);
  if ($month == 0) {
    // per year
    $table = '<tr><thead><th>'. $person_name .'</th>';
    for ($i=1;$i<=12;$i++) {
      $monthstr = t(date('M', strtotime($year .'-'. $i .'-1')));
      $table .= '<th><a href="'. url('employee_performance/'. $person_id .'/'. $year .'/'. $i) .'">'. $monthstr .'<a></th>';
    }
    $table .= '<th>'. t('Total') .'</th></thead></tr><tr class="odd"><td>'. t('Hours actual') .'</td>';
    for ($i=1;$i<=12;$i++) {
      $table .= '<td>'. round($data[$i]['actual'], 2) .'</td>';
    }
    $table .= '<td>'. round($data['total']['actual'], 2) .'</td></tr><tr class="even"><td>'. t('Hours must') .'</td>';
    for ($i=1;$i<=12;$i++) {
      $table .= '<td>'. round($data[$i]['must'], 2) .'</td>';
    }
    $table .= '<td>'. round($data['total']['must'], 2) .'</td></tr>';
    $table .= '<tr class="odd"><td>'. t('Difference') .'</td>';
    for ($i=1;$i<=12;$i++) {
      $table .= '<td>'. round($data[$i]['diff'], 2) .'</td>';
    }
    $table .= '<td>'. round($data['total']['diff'], 2) .'</td></tr><tr colspan="14" height="20" class="even"></tr><tr class="odd"><td>'. t('Holidays booked') .'</td>';
    for ($i=1;$i<=12;$i++) {
      $table .= '<td>'. round($data[$i]['holidays'], 1) .'</td>';
    }
    $table .= '<td>'. round($data['total']['holidays'], 1) .'</td></tr><tr class="even"><td>'. t('Holidays left') .'</td>';
    for ($i=1;$i<=12;$i++) {
      $table .= '<td>'. round($data[$i]['holidays_left'], 1) .'</td>';
    }
    $table .='<td>'. round($data['total']['holidays_left'], 1) .'</td></tr><tr colspan="14" height="20" class="odd"></tr><tr class="even"><td>'. t('Sick days') .'</td>';
    for ($i=1;$i<=12;$i++) {
      $table .= '<td>'. round($data[$i]['sickdays'], 1) .'</td>';
    }
    $table .='<td>'. round($data['total']['sickdays'], 1) .'</td></tr>';
  }
  else {
    // per month
    $table = '<tr><thead><th>'. $person_name .'</th>';
    for ($i=1;$i<=$data['days'];$i++) {
      $table .= '<th>'. $i .'<br />'. $data[$i]['weekday'] .'</th>';
    }
    $table .= '<th>'. t('Total') .'</th><thead></tr><tr class="odd"><td>'. t('Hours actual') .'</td>';
    for ($i=1;$i<=$data['days'];$i++) {
      $table .= '<td>'. round($data[$i]['actual'], 2) .'</td>';
    }
    $table .= '<td>'. round($data['total']['actual'], 2) .'</td></tr><tr class="even"><td>'. t('Hours must') .'</td>';
    for ($i=1;$i<=$data['days'];$i++) {
      $table .= '<td>'. round($data[$i]['must'], 2) .'</td>';
    }
    $table .= '<td>'. round($data['total']['must'], 2) .'</td></tr>';
    $table .= '<tr class="odd"><td>'. t('Difference') .'</td>';
    for ($i=1;$i<=$data['days'];$i++) {
      $table .= '<td>'. round($data[$i]['diff'], 2) .'</td>';
    }
    $table .= '<td>'. round($data['total']['diff'], 2) .'</td></tr><tr colspan="'. ($data['days'] + 2) .'" height="20" class="even"></tr><tr class="odd"><td>'. t('Holidays booked') .'</td>';
    for ($i=1;$i<=$data['days'];$i++) {
      $table .= '<td>'. round($data[$i]['holidays'], 1) .'</td>';
    }
    $table .= '<td>'. round($data['total']['holidays'], 1) .'</td></tr><tr class="even"><td>'. t('Holidays left') .'</td>';
    for ($i=1;$i<=$data['days'];$i++) {
      $table .= '<td>'. round($data[$i]['holidays_left'], 1) .'</td>';
    }
    $table .='<td>'. round($data['total']['holidays_left'], 1) .'</td></tr><tr colspan="'. ($data['days'] + 2) .'" height="20" class="odd"></tr><tr class="even"><td>'. t('Sick days') .'</td>';
    for ($i=1;$i<=$data['days'];$i++) {
      $table .= '<td>'. round($data[$i]['sickdays'], 1) .'</td>';
    }
    $table .='<td>'. round($data['total']['sickdays'], 1) .'</td></tr>';
  }
  return $table;
}
