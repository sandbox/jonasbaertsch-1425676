Purpose:
Extend the functionality of Stormtimetracking and get a yearly and monthly overview of the booked hrs per employee.

Documentation for employee performance module.
- Project homepage: http://drupal.org/sandbox/jonasbaertsch/1425676
- Project dependencies: http://drupal.org/project/storm
- Issue tracking: http://drupal.org/project/issues/1425676


--Installation--
Get the module and enable 
Create an organization to book holidays and sick days to (suggestion: create a duplicate of the host organization and append internal)

--Settings & configuration--
/admin/settings/employee_performance

1. Set the required number or hrs 
2. Set the number of days of holidays
3. Select a task to book the holidays to 
4. Select a task for sick days